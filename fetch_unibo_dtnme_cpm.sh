#!/bin/bash

## fetch_unibo_dtnme_cpm.sh
#
#  Utility script to fetch Unibo-DTNME-CPM (Contact Plan Manager).
#
#
#  Written by Lorenzo Persampieri:  lorenzo.persampieri@studio.unibo.it
#  Supervisor Prof. Carlo Caini:    carlo.caini@unibo.it
#
#  Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
#
#  This file is part of Unibo-DTNME-CPM.
#
#  Unibo-DTNME-CPM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Unibo-CGR is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Unibo-DTNME-CPM.  If not, see <http://www.gnu.org/licenses/>.
#
##

set -euo pipefail

function help_fun() {
#	1>&2 redirect standard output to standard error
	echo "Usage: $0 </path/to/Unibo-DTNME-CPM/> " 1>&2
	echo "This script fetch Unibo-DTNME-CPM from GitLab." 1>&2
}

if test $# -ne 1
then
	help_fun
	exit 1
fi

UNIBO_DTNME_CPM="$1"

if ! test -d "$UNIBO_DTNME_CPM"
then
	help_fun
	echo "$UNIBO_CGR_DTNME_CPM_ROOT is not a directory" >&2
	exit 1
fi

# replace special characters in URL format. example: replace "!" with "%20"
function url_encode() {
    echo "$@" \
    | sed \
        -e 's/%/%25/g' \
        -e 's/ /%20/g' \
        -e 's/!/%21/g' \
        -e 's/"/%22/g' \
        -e "s/'/%27/g" \
        -e 's/#/%23/g' \
        -e 's/(/%28/g' \
        -e 's/)/%29/g' \
        -e 's/+/%2b/g' \
        -e 's/,/%2c/g' \
        -e 's/-/%2d/g' \
        -e 's/:/%3a/g' \
        -e 's/;/%3b/g' \
        -e 's/?/%3f/g' \
        -e 's/@/%40/g' \
        -e 's/\$/%24/g' \
        -e 's/\&/%26/g' \
        -e 's/\*/%2a/g' \
        -e 's/\./%2e/g' \
        -e 's/\//%2f/g' \
        -e 's/\[/%5b/g' \
        -e 's/\\/%5c/g' \
        -e 's/\]/%5d/g' \
        -e 's/\^/%5e/g' \
        -e 's/_/%5f/g' \
        -e 's/`/%60/g' \
        -e 's/{/%7b/g' \
        -e 's/|/%7c/g' \
        -e 's/}/%7d/g' \
        -e 's/~/%7e/g'
}


function fetch_gitlab_file() {
    local PROJECT_ID="$1"
    local BRANCH="$2"
    local REMOTE_FILE=$(url_encode "$3")
    
    curl "https://gitlab.com/api/v4/projects/$PROJECT_ID/repository/files/$REMOTE_FILE/raw?ref=$BRANCH" --silent
}

function fetch_unibo_dtnme_file() {
    local REMOTE_FILE="$1"
    local PROJECT_ID="30169226"
    local BRANCH="master"

    fetch_gitlab_file "$PROJECT_ID" "$BRANCH" "$REMOTE_FILE"
}

function fetch_unibo_dtnme_cpm() {
    local UNIBO_DTNME_CPM="$1"
    echo "Fetch Unibo-DTNME-CPM files from GitLab"
    echo ""
    echo "Fetch top level files"
    fetch_unibo_dtnme_file "SourceListCPM.txt" > "$UNIBO_DTNME_CPM/SourceListCPM.txt"
    fetch_unibo_dtnme_file "mv_unibo_dtnme_cpm.sh" > "$UNIBO_DTNME_CPM/mv_unibo_dtnme_cpm.sh"
    chmod +x "$UNIBO_DTNME_CPM/mv_unibo_dtnme_cpm.sh"
    fetch_unibo_dtnme_file "contact-plan.txt" > "$UNIBO_DTNME_CPM/contact-plan.txt"

    echo "Fetch src/contact_plan files"
    mkdir -p "$UNIBO_DTNME_CPM/src/contact_plan"
    fetch_unibo_dtnme_file "src/contact_plan/CPContact.cc" > "$UNIBO_DTNME_CPM/src/contact_plan/CPContact.cc"
    fetch_unibo_dtnme_file "src/contact_plan/CPContact.h" > "$UNIBO_DTNME_CPM/src/contact_plan/CPContact.h"
    fetch_unibo_dtnme_file "src/contact_plan/CPRange.cc" > "$UNIBO_DTNME_CPM/src/contact_plan/CPRange.cc"
    fetch_unibo_dtnme_file "src/contact_plan/CPRange.h" > "$UNIBO_DTNME_CPM/src/contact_plan/CPRange.h"
    fetch_unibo_dtnme_file "src/contact_plan/ContactPlanManager.cc" > "$UNIBO_DTNME_CPM/src/contact_plan/ContactPlanManager.cc"
    fetch_unibo_dtnme_file "src/contact_plan/ContactPlanManager.h" > "$UNIBO_DTNME_CPM/src/contact_plan/ContactPlanManager.h"
    fetch_unibo_dtnme_file "src/contact_plan/ContactPlanTimer.cc" > "$UNIBO_DTNME_CPM/src/contact_plan/ContactPlanTimer.cc"
    fetch_unibo_dtnme_file "src/contact_plan/ContactPlanTimer.h" > "$UNIBO_DTNME_CPM/src/contact_plan/ContactPlanTimer.h"

    echo "Fetch src/bundling files"
    mkdir -p "$UNIBO_DTNME_CPM/src/bundling"
    fetch_unibo_dtnme_file "src/bundling/BundleEvent.h" > "$UNIBO_DTNME_CPM/src/bundling/BundleEvent.h"
    fetch_unibo_dtnme_file "src/bundling/BundleEventHandler.cc" > "$UNIBO_DTNME_CPM/src/bundling/BundleEventHandler.cc"
    fetch_unibo_dtnme_file "src/bundling/BundleEventHandler.h" > "$UNIBO_DTNME_CPM/src/bundling/BundleEventHandler.h"

    echo "Fetch src/routing files"
    mkdir -p "$UNIBO_DTNME_CPM/src/routing"
    fetch_unibo_dtnme_file "src/routing/TableBasedRouter.cc" > "$UNIBO_DTNME_CPM/src/routing/TableBasedRouter.cc"
    fetch_unibo_dtnme_file "src/routing/TableBasedRouter.h" > "$UNIBO_DTNME_CPM/src/routing/TableBasedRouter.h"

    echo "Fetching done"
}

fetch_unibo_dtnme_cpm "$UNIBO_DTNME_CPM"

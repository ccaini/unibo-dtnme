#!/bin/bash

## mv_unibo_dtnme_cpm.sh
#
#  Utility script to add Unibo-DTNME-CPM classes to DTNME.
#  Launch this script without arguments to get help.
#
#  Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
#
#  This file is part of Unibo-DTNME-CPM.
#
#  Unibo-DTNME-CPM is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Unibo-CGR is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Unibo-DTNME.  If not, see <http://www.gnu.org/licenses/>.
##

#set -euxo pipefail
set -euo pipefail

function help_fun() {
	echo "Usage: $0 /path/to/Unibo-DTNME-CPM /path/to/DTNME/>" 1>&2
	echo "This script includes support for scheduled contacts in DTNME." 1>&2
	echo "Launch this script with the parameter explicited in the Usage string." 1>&2
}

if test $# -ne 2
then
	help_fun
	exit 1
fi

UNIBO_DTNME_CPM="$1"
DTNME_DIR="$2"

if ! test -d "$UNIBO_DTNME_CPM"
then
        help_fun
        echo "$UNIBO_DTNME_CPM is not a directory" >&2
        exit 1
fi

if ! test -d "$DTNME_DIR"
then
	help_fun
	echo "$DTNME_DIR is not a directory" >&2
	exit 1
fi


function update_dtnme_servlib_makefile() {
	local SERVLIB_MAKEFILE="$1"
	local TO_ADD="$2"
	local SRCS_NAME="$3"

#	check if we tweaked the makefile in a previous run
	if grep -q "$SRCS_NAME" "$SERVLIB_MAKEFILE" ; then
		return
	fi

	local TEMP_FILE=".temp_servlib_makefile"
	touch "$TEMP_FILE"
	chown --reference="$SERVLIB_MAKEFILE" "$TEMP_FILE"

#	find first occurrence of "SERVLIB_SRCS" in Makefile
	local lineNum=$(grep -n 'SERVLIB_SRCS' "$SERVLIB_MAKEFILE" | cut -d":" -f 1 | head -n 1)

#	copy first part of Makefile
	head -n $(($lineNum-1)) "$SERVLIB_MAKEFILE" > "$TEMP_FILE"

#	insert section "$SRCS_NAME" with source filenames before SERVLIB_SRCS
	echo -e "$SRCS_NAME := \t\\" >> "$TEMP_FILE"
	while read srcLine; do
#	skip empty lines
	if test -z "$srcLine" ; then
		break
	fi
	echo -e "\t$srcLine\t\\"
	done < "$TO_ADD" >> "$TEMP_FILE"
	echo >> "$TEMP_FILE"

#	copy SERVLIB_SRCS line
	sed -n "$lineNum"'p' < "$SERVLIB_MAKEFILE" >> "$TEMP_FILE"

#	insert line "$(SRCS_NAME) \" in SERVLIB_SRCS
	echo -e "\t"'$('"$SRCS_NAME"')'"\t\t\t\t\\" >> "$TEMP_FILE"

#	copy remaining part of Makefile
	tail -n +$(($lineNum + 1)) "$SERVLIB_MAKEFILE" >> "$TEMP_FILE"

	mv "$TEMP_FILE" "$SERVLIB_MAKEFILE"
}


function mv_unibo_dtnme_cpm() {
	local UNIBO_DTNME_CPM="$1"
	local DTNME="$2"
	SRC="$UNIBO_DTNME_CPM/src"
	echo "Add Unibo-DTNME-CPM (Contact Plan Management) src files to DTNME"

	echo "Copy all Unibo-DTNME-CPM/src contact_plan files (4 classes)"
	rm -rf "$DTNME/servlib/contact_plan"
	cp -rpf "$SRC/contact_plan" "$DTNME/servlib/contact_plan"

	echo "Overwirite original TableBasedRouter"
	cp -pf "$SRC/routing/TableBasedRouter.cc" "$DTNME/servlib/routing"
	cp -pf "$SRC/routing/TableBasedRouter.h" "$DTNME/servlib/routing"

	echo "Overwrite original BundleEventHandler"
	cp -pf "$SRC/bundling/BundleEvent.h" "$DTNME/servlib/bundling"
	cp -pf "$SRC/bundling/BundleEventHandler.h" "$DTNME/servlib/bundling"
	cp -pf "$SRC/bundling/BundleEventHandler.cc" "$DTNME/servlib/bundling"

	echo "Copy contact-plan.txt"
	cp -pf "$UNIBO_DTNME_CPM/contact-plan.txt" "$DTNME"

        update_dtnme_servlib_makefile "$DTNME/servlib/Makefile" "$UNIBO_DTNME_CPM/SourceListCPM.txt" "UNIBO_DTNME_CPM_SRCS"
}

mv_unibo_dtnme_cpm "$UNIBO_DTNME_CPM" "$DTNME_DIR"


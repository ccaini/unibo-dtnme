/** \file CPContact.cc
 *
 *  \brief This file provides the definition of the CPRange class, a structure needed to
 *  		manage a range entry in the contact plan
 *
 ** \copyright Copyright (c) 2021, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of Unibo-DTNME-Extensions.                                            <br>
 **                                                                               <br>
 **    Unibo-DTNME-Extensions is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-DTNME-Extensions is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.
 *
 *  \author Gabriele Nunziati, gabriele.nunziati2@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */
#include <time.h>
#include <cstdint>
#ifndef SERVLIB_CONTACT_PLAN_CPRANGE_H_
#define SERVLIB_CONTACT_PLAN_CPRANGE_H_

namespace dtn {

/**
 * 	Definition of CPRange
 */
class CPRange {
    time_t startTime = 0;
    time_t endTime = 0;
    uint64_t from = 0;
    uint64_t to = 0;
    uint64_t delay = 0;

public:
    CPRange() {}
	CPRange(time_t,time_t,uint64_t, uint64_t, uint64_t);
    void setStartTime(time_t startTime_) noexcept { this->startTime = startTime_; };
    void setEndTime(time_t endTime_) noexcept { this->endTime = endTime_; };
    void setFrom(uint64_t from_) noexcept { this->from = from_; };
    void setTo(uint64_t to_) noexcept { this->to = to_; };
    void setDelay(uint64_t delay_) noexcept { this->delay = delay_; };
	time_t getStartTime() const noexcept {return startTime;};
	time_t getEndTime() const noexcept {return endTime;};
    uint64_t getFrom() const noexcept {return from;};
    uint64_t getTo() const noexcept {return to;};
    uint64_t getDelay() const noexcept {return delay;};

    // return true if two CPContact(s) have the same sender, receiver and start time
    // return false otherwise
    bool operator==(const CPRange& rhs) const noexcept {
        return from == rhs.from && to == rhs.to && startTime == rhs.startTime;
    }
    bool operator!=(const CPRange& rhs) const noexcept {
        return !(*this == rhs);
    }
};

}

#endif /* SERVLIB_CONTACT_PLAN_CPRANGE_H_ */

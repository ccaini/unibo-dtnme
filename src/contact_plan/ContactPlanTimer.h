/** \file ContactPlanTimer.h
 *
 *  \brief This file provides the definition of the functions
 *            to support the launch of a new Contact Plan Up event when
 *            there is an opening contact from the local node
 *
 ** \copyright Copyright (c) 2021, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of Unibo-DTNME-Extensions.                                            <br>
 **                                                                               <br>
 **    Unibo-DTNME-Extensions is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-DTNME-Extensions is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.
 *
 *  \author Gabriele Nunziati, gabriele.nunziati2@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */

#ifndef SERVLIB_CONTACT_PLAN_CONTACTPLANTIMER_H_
#define SERVLIB_CONTACT_PLAN_CONTACTPLANTIMER_H_

#include <../../third_party/oasys/thread/Timer.h>
#include <../../third_party/oasys/util/Time.h>
#include "../bundling/BundleDaemon.h"
#include "../bundling/BundleEvent.h"
#include <list>

namespace dtn {

class ContactPlanTimer;

typedef std::shared_ptr<ContactPlanTimer> SPtr_ContactPlanTimer;

class ContactPlanTimer : public oasys::SharedTimer {
public:

    /**
     * Constructor
     */
	ContactPlanTimer();

    /**
     * Destructor
     */
    virtual ~ContactPlanTimer() {}

    /**
     * schedule the timer
     */
    virtual void start();

    /**
     * Virtual timeout function
     */
    virtual void timeout(const struct timeval& now) override;

    /**
     * Cancels the timer
     */
    virtual void cancel();

protected:

    //List of the opening contacts opening
    std::list<CPContact> opening_contacts;

    oasys::SPtr_Timer sptr_timer;      ///< internal reference to this timer prevents untimely deletion

};

}

#endif /* SERVLIB_CONTACT_PLAN_CONTACTPLANTIMER_H_ */

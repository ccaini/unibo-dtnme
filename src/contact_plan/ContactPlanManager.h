/** \file ContactPlanManager.h
 *
 *  \brief This file provides the definition of the functions
 *            to support deterministic contacts on DTNME
 *
 ** \copyright Copyright (c) 2021, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
** \par License
 **
 **    This file is part of Unibo-DTNME-Extensions.                                            <br>
 **                                                                               <br>
 **    Unibo-DTNME-Extensions is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-DTNME-Extensions is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with Unibo-DTNME-Extensions.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Gabriele Nunziati, gabriele.nunziati2@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */

#include "CPContact.h"
#include "CPRange.h"
#include <memory>
#include <list>
#include <iostream>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <string>
#include "../bundling/BundleDaemon.h"
#include "ContactPlanTimer.h"
#include <third_party/oasys/thread/SpinLock.h>
#include <third_party/oasys/debug/DebugUtils.h>

#ifndef REMOVE
/**
 * \brief Boolean: Set to 1 if you want some remove all ranges and contacts when contact-plan is updated
 *
 * \hideinitializer
 */
#define REMOVE 0
#endif



#ifndef SOURCES_CONTACT_MANAGER_H_
#define SOURCES_CONTACT_MANAGER_H_

namespace dtn {

	/**
	 * 	Definition of ContactPlanManager class
	 */
class ContactPlanManager {

    /**
     * Valid states for ContactPlanManager
     */
    typedef enum
    {
        LOADING_ERROR = -2,
        /**
         *	Errors encountered during initialization of the class
         */
        SUCCESFULLY_LOADED = 1,
        /**
         * The contact plan was successfully loaded and the class is ready to use
         */
        UNINITIALIZED = -1,
        /**
         * The class is still to be initialized
         */
    }
    CPMState;

    typedef enum
    {
    	UNIBOCGR = 1,
		/**
		 * The class is called by interface_unibocgr_dtn2.cc
		 */
    	STATIC = 2,
		/**
		 * The class is called by TableBasedRouter.cc
		 */
    }
    Caller_t;

    Caller_t CALLER;

    // Variable for the state of the class
	CPMState STATE;

	// Ipn value of the local node
	int localNode;

	// List of the contacts
	std::list<CPContact> contactList;

	// List of the ranges
	std::list<CPRange> rangeList;

	// Number of entries
	int totalEntries;

	// Name of the contact file
	char CONTACT_FILE[256];

	// Local node
	dtn::EndpointID localEid;

	// Timestamp of the creation of the class, used to
	//calculate whether a contact is open or not
	//since they are expressed in differential time
    // seconds elapsed since Unix Epoch (01.01.1970 00:00:00 UTC)
	time_t timeZero;

	//Timestamp of the latest update of ContactList and RangeList
	struct timeval lastUpdateList;

	//Timestamp of the latest update of CGR's structes
	struct timeval lastUpdateCGR;

	// Reference to the timer used to launch a new event
	//in case a contact opens
	ContactPlanTimer * nextTimer;

	/**
	 * Lock to protect internal data structures.
	 */
	mutable oasys::SpinLock lock_;

   /*
    * Constructor
    *
    * Private.
    */
    ContactPlanManager();

public:
	/*
	 * Destructor
	 */
	~ContactPlanManager();

    static ContactPlanManager* instance();

    /*
     * First, check if the contact plan file has been updated.
     * In affirmative case load it and return true.
     * In negative case check if "target" time is different from the internal
     * last update time: return true if different and return false if equal.
     *
     * If the return value is true target will be updated to match the internal update time.
     */
    bool check_for_updates(struct timeval* target);

    time_t get_time_zero() { return timeZero; }

	/*
	 * Delete old contacts from the lists
	 */
	int delete_old_entries();

	/*
	 *	Print the loaded contacts in a file
	 */
	void print_contact_plan();

	/*
	 * Get how many seconds passed since the
	 * creation of the class
	 */
	int get_seconds_from_time_zero();

	/*
	 * Get the ipn of a EID
	 */
	int get_ipn(dtn::EndpointID remoteEid);

	/*
	 * Check if an eid uses the ipn scheme
	 */
	bool is_ipn(dtn::EndpointID eid);

	/*
	 * Check if there is an open contact now to a certain receiver
	 */
	bool is_there_open_contact_now(int receiver);

	/*
	 * Check if there is an open contact now to a certain receiver
	 */
	bool is_there_open_contact_now(dtn::EndpointID remoteEid);

	/*
	 * Check if there is an open contact to a certain receiver at a certain time
	 */
	bool is_there_open_contact(int receiver, int time);

	/*
	 * Get the remaining volume of data to a certain receiver
	 */
	long get_residual_volume(int receiver);

	/*
	 * Get the remaining volume of data to a certain receiver
	 */
	long get_residual_volume(dtn::EndpointID remoteEid);

	/*
	 * Create a new time to launch a new event when a contact from the local
	 * node opens
	 */
	void new_timer();

	/*
	 * This function returns the most immediate opening time
	 * of a contact in the plan
	 */
	int get_next_start_time();

	/*
	 * This function returns the most immediate closing time
	 * of a contact in the plan
	 */
	int get_next_end_time();

	/*
	 * This function return a list of contacts which are opening
	 * in a given time
	 */
	std::list<CPContact> get_opening_contacts(int when);

	/*
	 * Get a contact with that sender and receiver and open at a certain time
	 */
	CPContact * find_contact(int sender, int receiver, int time);

	/*
	 * Get a range with that sender and receiver and open at a certain time
	 */
	CPRange * find_range(int sender, int receiver, int time);

	/*
	 * Get a copy of the contact list
	 */
	std::list<CPContact> get_contact_list();

	/*
	 * Get a copy of the range list
	 */
	std::list<CPRange> get_range_list();

	/*
	 * Read the contact-plan
	 */
	int read_contact_plan(char * file);

	/*
	 * Check if structes are updated comapared to list
	 */
	bool isCGRupdateComparedList();
private:

    void initialize();

    /*
     * Load a new contact plan from a new file
     */
    int update_contact_plan(char * file);

	bool check_file_existance(char * file);
	int add_contact_to_list(time_t startTimePar, time_t endTimePar, unsigned long long fromPar, unsigned long long toPar,long unsigned int transmissionSpeedPar);
	int add_range_to_list(time_t startTimePar, time_t endTimePar, unsigned long long fromPar, unsigned long long toPar, unsigned int delayPar);
	int read_file_contactranges(char * filename);
	int add_contact(char* fileline);
	int add_range(char* fileline);
	bool areListUpdated();
};

}

#endif /* SOURCES_CONTACT_MANAGER_H_ */

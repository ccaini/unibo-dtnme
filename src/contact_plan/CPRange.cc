/** \file CPContact.cc
 *
 *  \brief This file provides the implementation of the CPRange class, a structure needed to
 *  		manage a range entry in the contact plan
 *
 ** \copyright Copyright (c) 2021, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of Unibo-DTNME-Extensions.                                            <br>
 **                                                                               <br>
 **    Unibo-DTNME-Extensions is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-DTNME-Extensions is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.
 *
 *  \author Gabriele Nunziati, gabriele.nunziati2@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */

#include "CPRange.h"

namespace dtn {

/*
 * CPRange constructor
 */
CPRange::CPRange(time_t startTimePar, time_t endTimePar, uint64_t fromPar, uint64_t toPar, uint64_t delayPar) {
	startTime = startTimePar;
	endTime = endTimePar;
	from = fromPar;
	to = toPar;
	delay = delayPar;
}

}



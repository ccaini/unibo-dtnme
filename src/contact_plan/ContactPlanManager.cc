/** \file 	ContactPlanManager.cc
 *
 *  \brief    This file provides the implementation of the functions
 *            to support deterministic contacts on DTNME
 *
 *  \details  This file contains the ContactPlanManager class which is
 *  			responsible for handling contacts from the contact plan
 *
 ** \copyright Copyright (c) 2021, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of Unibo-DTNME-Extensions.                                            <br>
 **                                                                               <br>
 **    Unibo-DTNME-Extensions is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-DTNME-Extensions is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.
 *
 *  \author Gabriele Nunziati, gabriele.nunziati2@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */

#include "ContactPlanManager.h"
#include <iostream>
#include <memory>
#include <cinttypes>

namespace dtn {

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::ContactPlanManager
 *
 * \brief  Constructor of the ContactPlanManager class
 *
 *
 * \par Date Written: 29/03/21
 *
 * \return  *ContactPlanManager Pointer to the class instance
 *
 * \warning file   There must be a contact-plan.txt file
 * \warning The file must be in the same directory in which dtnme is called
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  04/04/21 | Gabriele Nunziati
 *  17/09/21 | Gabriele Nunziati
 *  20/03/22 | Federico Le Pera| Add const char* type in the signature of constructor to initialize
 *  						   | the enum CALL to STATIC or UNIBOCGR
 *****************************************************************************/

//----------------------------------------------------------------------
ContactPlanManager::ContactPlanManager() {
    STATE = UNINITIALIZED;
    strcpy(CONTACT_FILE,"contact-plan.txt");
    time(&timeZero);
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::~ContactPlanManager
 *
 * \brief  Destructor of the ContactPlanManager class
 *
 *
 * \par Date Written: 29/03/21
 *
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  29/03/21 | Gabriele Nunziati
 *  15/09/21 | Gabriele Nunziati | Added timer destruction
 *****************************************************************************/

//----------------------------------------------------------------------
ContactPlanManager::~ContactPlanManager() {
	if (nextTimer) {
		nextTimer->clear_pending();
	}
}

ContactPlanManager* ContactPlanManager::instance() {
    static std::unique_ptr<ContactPlanManager> obj(new ContactPlanManager());
    return obj.get();
}

void ContactPlanManager::initialize() {
    if (STATE == SUCCESFULLY_LOADED) return;

    localEid = dtn::BundleDaemon::instance()->local_eid_ipn();
    if(is_ipn(localEid))
        localNode = ContactPlanManager::get_ipn(localEid);
    else {
        localNode = 0;
        STATE = SUCCESFULLY_LOADED;
        // we handle this case in the same way as like as the contact plan was empty
        gettimeofday(&lastUpdateList, NULL);
        print_contact_plan();
        return;
    }

    int result = read_contact_plan(CONTACT_FILE);

    totalEntries = result;
    //Print the plan in a debug file
    print_contact_plan();
    new_timer();

    STATE = SUCCESFULLY_LOADED;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::read_contact_plan
 *
 * \brief  This function check the file existance and then calls
 * 			the function read_file_contactranges() which actually
 * 			read the contacts
 *
 *
 * \par Date Written:
 *      18/09/2021
 *
 * \return int Number of entries (both contacts and ranges) read
 *
 * \retval    0   Empty file
 * \retval   -1   Loading error
 *
 * \param[in]     file              File name containing the contact plan
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 18/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 * 20/03/22  | Federico Le Pera |  Support to interface_unibo_CGR
 *  03/11/22 | L. Persampieri  |   Remove if (CALLER == UNIBOCGR) { ... }
 *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::read_contact_plan(char * file) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::read_contact_plan");

	int result = -1;

	if(ContactPlanManager::check_file_existance(file)) {
        result = ContactPlanManager::read_file_contactranges(file);
        gettimeofday(&lastUpdateList, NULL);
        new_timer();
    }

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::check_file_existance
 *
 * \brief  This function check the file existance
 *
 * \par Date Written:
 *      18/09/2021
 *
 * \return bool
 *
 * \retval   true  File exists
 *
 * \warning	if the file doesn't exist, the application will panic
 *
 * \param[in]     file              File name containing the contact plan
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  18/09/21 | Gabriele Nunziati |  Initial Implementation and documentation.
 *  03/11/22 | L. Persampieri | Removed PANIC().
 *****************************************************************************/

//----------------------------------------------------------------------
bool ContactPlanManager::check_file_existance(char * file) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::check_file_existance");

	if (FILE *fd = fopen(file, "r")) {
        fclose(fd);
        return true;
    } else {
        return false;
    }
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::update_contact_plan
 *
 * \brief  This function will re-read the entries from the contact file,
 *			using the file whose name is passed as a parameter
 *
 * \par Date Written:
 *      29/03/2021
 *
 * \return int	Number of read entries from the file
 *
 * \retval 	0	Empty file
 * \retval  -1	Loading error
 * \retval -2 The List are updated compared the latest timestamp of contact plan
 *
 * \param[in]	file	char * describing the file name
 *
 * \warning	if the file doesn't exist, the application will panic
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 * 1/05/2022 | Federco Le Pera | Support for update only if it necessary
 *  03/11/22 | L. Persampieri  | Make function private.
 *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::update_contact_plan(char * file){
	oasys::ScopeLock l(&lock_, "ContactPlanManager::update_contact_plan");

	int result = -1;
	if(areListUpdated()==true) return result = -3;
	contactList.clear();
	rangeList.clear();
    result = read_contact_plan(CONTACT_FILE);
    totalEntries = 0;
    if (result >= 0) {
        totalEntries = result;
    }
	return result;
}

bool ContactPlanManager::check_for_updates(struct timeval *target) {
    oasys::ScopeLock l(&lock_, "ContactPlanManager::check_for_updates");
    initialize();
    int result = update_contact_plan(CONTACT_FILE);
    // if a new contact plan has been loaded from file or the target time does not match the latest update time
    if (result >= 0 || target->tv_sec != lastUpdateList.tv_sec || target->tv_usec != lastUpdateList.tv_usec) {
        target->tv_sec = lastUpdateList.tv_sec;
        target->tv_usec = lastUpdateList.tv_usec;
        return true;
    }
    return false;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::delete_old_entries
 *
 * \brief  This function will delete old entries in the contacts list
 * 			and the ranges list
 *
 * \par Date Written:
 *      18/09/2021
 *
 * \return int	Number of deleted entries from the file
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::delete_old_entries(void) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::delete_old_entries");

	std::list<CPContact>::iterator itList;
	int now = get_seconds_from_time_zero();

	int endTime;
	int totDeleted = 0;
	for(itList = contactList.begin(); itList != contactList.end(); itList++) {
		endTime = (*itList).getEndTime();

		if(endTime < now) {
			itList = contactList.erase(itList);
			totDeleted++;
		}
	}

	print_contact_plan();

	return totDeleted;
}
/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::print_contact_plan
 *
 * \brief  This function will print the read contact plan in a
 * 			contact-debug.txt
 *
 * \par Date Written:
 *      19/05/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 23/05/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 * 13/09/2021 | Gabriele Nunziati | Added debug file
 *****************************************************************************/

//----------------------------------------------------------------------
void ContactPlanManager::print_contact_plan() {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::print_contact_plan");

	std::list<CPContact>::iterator itc;
	std::list<CPRange>::iterator itr;

	FILE * fd = fopen("contact_debug.txt","w");

	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime (&rawtime);
	fprintf (fd,"%s\nLocal Node Ipn: %i\n", asctime(timeinfo),localNode);

	fprintf(fd,"------------------- LOADED CONTACTS: -------------------\n");
	fprintf(fd,"Start Time	End Time	From	To	TransmissionSpeed\n");
	for(itc = contactList.begin(); itc != contactList.end(); itc++) {
		fprintf(fd, "%" PRIu64 "		%" PRIu64 "		%" PRIu64 "	%" PRIu64 "	%" PRIu64 "\n", (*itc).getStartTime(), (*itc).getEndTime(),
				(*itc).getFrom(), (*itc).getTo(), (*itc).getTransmissionSpeed() );
	}
	fprintf(fd,"--------------------------------------------------------\n");

	fprintf(fd,"\n------------------- LOADED RANGES: ---------------------\n");
	fprintf(fd,"Start Time	End Time	From	To	Delay\n");
	for(itr = rangeList.begin(); itr != rangeList.end(); itr++) {
		fprintf(fd,"%" PRIu64 "		%" PRIu64 "		%" PRIu64 "	%" PRIu64 "	%" PRIu64 "\n", (*itr).getStartTime(), (*itr).getEndTime(),
				(*itr).getFrom(), (*itr).getTo(), (*itr).getDelay() );
	}
	fprintf(fd,"--------------------------------------------------------\n");

	fclose(fd);
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_seconds_from_time_zero
 *
 * \brief  This function will return an integer representing the seconds passed
 * 			since the initialization of this class
 *
 * \details Contacts in the file presents differential times: time is expressed as a
 * 			difference from the moment the class is created. Hence, a function which
 * 			tells how many seconds passed since the creation of the class is need to
 * 			check if a contact is open or not
 *
 * \return int	seconds from time zero
 *
 * \par Date Written:
 *      19/05/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 23/05/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::get_seconds_from_time_zero() {

	// return currentTime - timeZero
	time_t currentTime;
	time(&currentTime);

	double result = difftime(currentTime,timeZero);
	int resultInt = (int) result;

	return resultInt;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_ipn
 *
 * \brief  This function will return an integer representing the ipn node of an EID
 *
 * \details Contacts in the lists present sender/receiver nodes as integers
 *
 * \warning BEFORE calling this function, it must be checked that the node is compatible
 * 			with the ipn scheme through the is_compatible(dtn::EndpointID) function of this class.
 *
 *\param[in]	dtn::RemoteEID	EID
 *
 *\return	int	ipn of the EID
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::get_ipn(dtn::EndpointID remoteEid) {
	std::string ipnName = remoteEid.str();
	std::string delimiter1 = ":";
	std::string delimiter2 = ".";
	std::string s = ipnName.substr(ipnName.find(delimiter1) + 1, ipnName.find(delimiter2) - 1);
	std::stringstream convert;
	long remoteNode;
	convert << s;
	convert >> remoteNode;

	if(remoteNode == 0)
		return -1;

	int receiver = (int) remoteNode;
	return receiver;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::is_compatible
 *
 * \brief  This function will check if a dtn::RemoteEID is compatible with the CPManager
 *
 * \details EID must use the ipn scheme to be used by this class
 *
 *\param[in]	dtn::RemoteEID	EID
 *
 *\return	bool	whether it's compatible (true) or not (false)
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
bool ContactPlanManager::is_ipn(dtn::EndpointID eid) {
	std::string remote_eid_name = eid.str();

	std::string dtnNone ("dtn:none");
	std::string ipnString ("ipn");

	std::size_t found = remote_eid_name.find(ipnString);

	if(dtnNone.compare(remote_eid_name) != 0 && found != std::string::npos)
		return true;

	return false;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::is_there_open_contact_now
 *
 * \brief  This function will check if there is an open contact from the local node
 * 			to the remote node (specified as a parameter) in the moment this function is called
 *
 * \warning It tells if there is a contact NOW, intended as the moment in which the
 * 			function is called
 *
 *\param[in]	int	ipn of the remote node
 *
 *\return	bool	Whether there is a contact (true) or not (false)
 *
 * \par Date Written:
 *      15/05/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/05/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
bool ContactPlanManager::is_there_open_contact_now(int receiver) {
	initialize();
	int time = ContactPlanManager::get_seconds_from_time_zero();

	CPContact * cpContact = find_contact(localNode, receiver, time);
	if(cpContact == NULL)
		return false;

	return true;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::is_there_open_contact_now
 *
 * \brief  This function will check if there is an open contact from the local node
 * 			to the remote node (specified as a parameter) in the moment this function is called
 *
 *\details	This version accept a dtn::EndpointID as a parameter representing the
 *			remote node
 *
 * \warning It tells if there is a contact NOW, intended as the moment in which the
 * 			function is called
 * \warning BEFORE calling this function, it must be checked that the remoteEID is compatible
 * 			with the ipn scheme through the is_compatible(dtn::EndpointID) function of this class.
 *
 *\param[in]	dtn::EndpointID	remote node
 *
 *\return	bool	Whether there is a contact (true) or not (false)
 *
 * \par Date Written:
 *      13/05/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 13/05/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
bool ContactPlanManager::is_there_open_contact_now(dtn::EndpointID remoteEid) {
	int receiver = ContactPlanManager::get_ipn(remoteEid);
	bool result = ContactPlanManager::is_there_open_contact_now(receiver);
	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::is_there_open_contact
 *
 * \brief  This function will check if there is an open contact from the local node
 * 			to the remote node (specified as a parameter) in the moment specified as
 * 			a time parameter
 *
 *\param[in]	int	ipn of the remote node
 *\param[in]	int	time in seconds since the creation of the class
 *
 *\return	bool	Whether there is a contact (true) or not (false)
 *
 * \par Date Written:
 *      15/05/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/05/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
bool ContactPlanManager::is_there_open_contact(int receiver, int time) {

	CPContact * cpContact = find_contact(localNode, receiver, time);
	if(cpContact == NULL)
		return false;

	return true;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_remaining_volume
 *
 * \brief  This function will return the remaining volume to a certain node specified
 * 			as a parameter
 *
 *\details	Ranges are also considered to calculate the volume
 *\details	If there is no contact to the node specified, the volume returned is 0
 *
 *\param[in]	int	ipn of the remote node
 *
 *\return	int	bytes representing the remaining volume
 *
 * \par Date Written:
 *      15/05/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/05/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
long ContactPlanManager::get_residual_volume(int receiver) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::get_remaining_volume");

	bool isThere = ContactPlanManager::is_there_open_contact_now(receiver);

	if(!isThere)
		return 0;

	int time = ContactPlanManager::get_seconds_from_time_zero();
	CPContact * contact = ContactPlanManager::find_contact(localNode, receiver, time);
	CPRange * range = ContactPlanManager::find_range(localNode, receiver, time);

	long transmissionSpeed = contact->getTransmissionSpeed();
	int delay = (int) range->getDelay();

	int endTime = contact->getEndTime();
	int finalEndTime = endTime - delay;

	long remainingVolume = transmissionSpeed * (finalEndTime - time);
	return remainingVolume;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_remaining_volume
 *
 * \brief  This function will return the remaining volume to a certain node specified
 * 			as a parameter
 *
 *\details	Ranges are also considered to calculate the volume
 *\details	If there is no contact to the node specified, the volume returned is 0
 * \warning BEFORE calling this function, it must be checked that the remoteEID is compatible
 * 			with the ipn scheme through the is_compatible(dtn::EndpointID) function of this class.
 *
 *\param[in]	dtn::EndpointID	remote node
 *
 *\return	int	bytes representing the remaining volume
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
long ContactPlanManager::get_residual_volume(dtn::EndpointID remoteEid) {
	int receiver = ContactPlanManager::get_ipn(remoteEid);
	long result = ContactPlanManager::get_residual_volume(receiver);
	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_next_start_time
 *
 * \brief  This function will return the moment in which the next opening of a contact from the local node
 * 			 is occurring
 *
 *\return	int	moment of the next opening (in differential time)
 *\return	-1	there are no more openings
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::get_next_start_time() {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::get_next_start_time");

	std::list<CPContact>::iterator it;

	int nextStart = -1;
	int startTime, from;
	int timeFromZero = ContactPlanManager::get_seconds_from_time_zero();
	for(it = contactList.begin(); it != contactList.end(); it++) {
		startTime = (*it).getStartTime();
		from = (*it).getFrom();

		//Consider only contacts from the local node
		if(from != localNode)
			continue;

		//Consider only contacts opening after the moment
		//this function is called
		if(startTime <= timeFromZero)
			continue;

		//nextStart is the start time of the first contact
		//or the most immediate among all contacts
		if(nextStart == -1 && startTime != 0)
			nextStart = startTime;
		if(startTime < nextStart && startTime != 0)
			nextStart = startTime;
	}

	return nextStart;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_next_end_time
 *
 * \brief  This function will return the moment in which the next closure of a contact from the local node
 * 			 is occurring
 *
 *\return	int	moment of the next closure (in differential time)
 *\return	-1	there are no more closures
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::get_next_end_time(void) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::get_next_end_time");

	std::list<CPContact>::iterator it;

	int nextEnd = -1;
	int endTime, from;
	int timeFromZero = ContactPlanManager::get_seconds_from_time_zero();
	for(it = contactList.begin(); it != contactList.end(); it++) {
		endTime = (*it).getEndTime();
		from = (*it).getFrom();

		//Consider only contacts from the local node
		if(from != localNode)
			continue;

		//Consider only contacts closing before the moment
		//this function is called
		if(timeFromZero < endTime)
			continue;

		//nextEnd is the end time of the first contact
		//or the most immediate among all contacts
		if(nextEnd == -1 && endTime != 0)
			nextEnd = endTime;
		if(endTime < nextEnd && endTime != 0)
			nextEnd = endTime;
	}

	return nextEnd;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_opening_contacts
 *
 * \brief  This function will return a list of contacts which open in a given moment
 * 			specificed as a parameter
 *
 *\param[in]	int	when the contacts are opening
 *
 *\return	std::list<CPContact>	list of the opening contacts
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
std::list<CPContact> ContactPlanManager::get_opening_contacts(int when) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::get_opening_contacts");

	std::list<CPContact>::iterator itList;
	std::list<CPContact> returnList;

	CPContact * cpContact;
	int startTime, from;
	for(itList = contactList.begin(); itList != contactList.end(); itList++) {
		startTime = (*itList).getStartTime();
		from = (*itList).getFrom();

		if(from != localNode || startTime != when)
			continue;

		cpContact = new CPContact((*itList).getStartTime(), (*itList).getEndTime(),
				(*itList).getFrom(), (*itList).getTo(), (*itList).getTransmissionSpeed());

		returnList.push_back(*cpContact);
	}

	return returnList;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::new_timer
 *
 * \brief  This function will instantiate a new timer for the purpose of launching a
 * 			new event when a contact from the local node is opening
 *
 * \par Date Written:
 *      15/09/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 15/09/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
void ContactPlanManager::new_timer(void) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::new_timer");

	nextTimer = new ContactPlanTimer();
	nextTimer->start();
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::find_contact
 *
 * \brief  This function will return a pointer to a contact which presents the same
 * 			sender and receiver as the parameters and is open in the moment specified
 * 			as a parameter
 *
 * \param[in]	int sender
 * \param[in]	int receiver
 * \param[int]	int time in which the contact wanted must be open
 *
 * \return CPContact*	pointer to the contact
 * \return NULL	if there is no contact with that sender, that receiver and that is open in that time
 *
 * \par Date Written:
 *      29/03/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
CPContact * ContactPlanManager::find_contact(int sender, int receiver, int time) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::find_contact");


	//If there is something to delete, delete before finding the contact
	delete_old_entries();

	std::list<CPContact>::iterator it;

	int startTime, endTime, from, to;
	for(it = contactList.begin(); it != contactList.end(); it++) {
		startTime = (*it).getStartTime();
		endTime = (*it).getEndTime();
		from = (*it).getFrom();
		to = (*it).getTo();

		//Returns the pointer to the CPContact
		//pointed by the iterator
		if(time >= startTime && time <= endTime && from == sender && to == receiver)
			return &(*it);
	}

	return NULL;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::find_range
 *
 * \brief  This function will return a pointer to a range which presents the same
 * 			sender and receiver as the parameters and is open in the moment specified
 * 			as a parameter
 *
 * \param[in]	int sender
 * \param[in]	int receiver
 * \param[int]	int time in which the range wanted is valid
 *
 * \return CPRange*	pointer to the range
 * \return NULL	if there is no range with that sender, that receiver and that is valid in that time
 *
 * \par Date Written:
 *      29/03/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
CPRange * ContactPlanManager::find_range(int sender, int receiver, int time) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::find_range");

	//If there is something to delete, delete before finding the range
	delete_old_entries();

	std::list<CPRange>::iterator it;

	int startTime, endTime, from, to;
	for(it = rangeList.begin(); it != rangeList.end(); it++) {
		startTime = (*it).getStartTime();
		endTime = (*it).getEndTime();
		from = (*it).getFrom();
		to = (*it).getTo();

		//Returns the pointer to the CPContact
		//pointed by the iterator
		if(time >= startTime && time <= endTime && from == sender && to == receiver)
			return &(*it);
	}

	return NULL;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_contact_list
 *
 * \brief  This function will return a copy of the list of the contacts
 *
 * \warning Mind that the list can be empty if there are no loaded contacts
 *
 * \return std::list<CPContact>	the list
 *
 * \par Date Written:
 *      29/03/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  03/11/22 | L. Persampieri   |  Copy list by means of copy constructor.
 *  *****************************************************************************/

//----------------------------------------------------------------------
std::list<CPContact> ContactPlanManager::get_contact_list(){
	oasys::ScopeLock l(&lock_, "ContactPlanManager::get_contact_list");

	//If there is something to delete, delete before returning the list
	delete_old_entries();

	return contactList; // call copy constructor
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::get_range_list
 *
 * \brief  This function will return a copy of the list of the ranges
 *
 * \warning Mind that the list can be empty if there are no loaded ranges
 *
 * \return std::list<CPRange>	the list
 *
 * \par Date Written:
 *      29/03/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  03/11/22 | L. Persampieri   |  Copy list by means of copy constructor.
 *  *****************************************************************************/

//----------------------------------------------------------------------
std::list<CPRange> ContactPlanManager::get_range_list(){
	oasys::ScopeLock l(&lock_, "ContactPlanManager::get_range_list");

	//If there is something to delete, delete before returning the list
	delete_old_entries();

	return rangeList; // call copy constructor
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::add_contact_to_list
 *
 * \brief  This function will create a new contact with the given parameters and add it to the
 * 			contacts list
 *
 * \warning TRANSMISSION SPEED must be ALWAYS IN Byte/s and NOT in Bit/s
 *
 *\param[in]	int start time
 *\param[in]	int end time
 *\param[in]	int from
 *\param[in]	int to
 *\param[in]	long transmission speed
 *
 * \return int
 * \return 1	contact succesfully added
 * \return -1	error: contact not added
 *
 * \par Date Written:
 *      29/03/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::add_contact_to_list(time_t startTimePar, time_t endTimePar, unsigned long long fromPar, unsigned long long toPar,long unsigned int transmissionSpeedPar) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::add_contact_to_list");

	int size = contactList.size();
	CPContact cpContact(startTimePar, endTimePar, fromPar, toPar, transmissionSpeedPar);
	contactList.push_back(cpContact);
	if(contactList.size() == (size+1) ) {
			totalEntries++;
			return 1;
	}
	else
		return -1;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanManager::add_range_to_list
 *
 * \brief  This function will create a new contact with the given parameters and add it to the
 * 			contacts list
 *
 * \warning DELAY must be ALWAYS IN Bytes and NOT Bits
 * \warning If in a range the from node ipn is less than the to node ipn, the range is made
 * 			bidirectional, adding another istance of the same range with inverted to and from fields
 *
 *\param[in]	int start time
 *\param[in]	int end time
 *\param[in]	int from
 *\param[in]	int to
 *\param[in]	long transmission speed
 *
 * \return int
 * \return 1	range succesfully added
 * \return -1	error: range not added
 *
 * \par Date Written:
 *      29/03/2021
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | --------------- 	| -----------------------------------------------
 * 29/03/2021| Gabriele Nunziati|  Initial Implementation and documentation.
 *  *****************************************************************************/

//----------------------------------------------------------------------
int ContactPlanManager::add_range_to_list(time_t startTimePar, time_t endTimePar, unsigned long long fromPar, unsigned long long toPar, unsigned int delayPar) {
	oasys::ScopeLock l(&lock_, "ContactPlanManager::add_range_to_list");

	int size = rangeList.size();

	CPRange cpRange(startTimePar, endTimePar, fromPar, toPar, delayPar);
	rangeList.push_back(cpRange);

	if(fromPar < toPar) {
		CPRange cpRangeOtherDirection(startTimePar, endTimePar, toPar, fromPar, delayPar);
		rangeList.push_back(cpRangeOtherDirection);
		size++;
	}

	if(rangeList.size() == (size+1) ) {
		totalEntries++;
		return 1;
	}
	else
		return -1;
}

/******************************************************************************
 *
 * \par Function Name:
 *      add_contact
 *
 * \brief  This function parses a string to get data of a contact and pass them to
 * 			the add_contact_to_list function
 *
 * \details This function was adapted from the Unibo-CGR/DTN2 interface made by Giacomo Gori
 *
 *
 * \par Date Written:
 *      02/07/20
 *
 * \return int
 *
 * \retval   1   Contact added
 * \retval   0   Contact's arguments error
 * \retval  -1   The contact overlaps with other contacts
 * \retval  -2   Can't read all info of the contact
 * \retval  -3   Can't read all info of the contact
 * \retval  -4   Can't read fromTime or toTime
 *
 * \param[in]   fileline	The line read from the file
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  02/07/20 | G. Gori		   |  Initial Implementation and documentation.
 *  22/03/21 | Gabriele Nunziati	| Support for ContactPlanManager
 *  21/03/21 | Federico Le Pera|  Support for use by interface_unibocgr_dtn2.cc
 *****************************************************************************/
int ContactPlanManager::add_contact(char * fileline)
{
	oasys::ScopeLock l(&lock_, "ContactPlanManager::add_contact");

	//Contact CgrContact;
	int count = 10, n=0, result = -2;
	long long fn = 0;
	long unsigned int xn = 0;
	//CgrContact.type = Scheduled;
	//fromTime
	unsigned long long from, to;
	time_t startTime, endTime;
	long unsigned int transmissionSpeed;
	if(fileline[count] == '+')
	{
		count++;
		while(fileline[count] >= 48 && fileline[count] <= 57) {
			n = n * 10 + fileline[count] - '0';
			count++;
		}
		count++;
		startTime = n;
		n=0;
	}
	else result = -4;
	//toTime
	if(fileline[count] == '+')
	{
		count++;
		while(fileline[count] >= 48 && fileline[count] <= 57) {
			n = n * 10 + fileline[count] - '0';
			count++;
		}
		count++;
		endTime = n;
		n=0;
	}
	else result = -4;
	//fromNode
	while(fileline[count] >= 48 && fileline[count] <= 57) {
			fn = fn * 10 + fileline[count] - '0';
			count++;
		}
	count++;
	from = fn ;
	fn=0;
	//toNode
	while(fileline[count] >= 48 && fileline[count] <= 57) {
			fn = fn * 10 + fileline[count] - '0';
			count++;
		}
	count++;
	to = fn ;
	fn=0;
	//txrate
	while(fileline[count] >= 48 && fileline[count] <= 57) {
			xn = xn * 10 + fileline[count] - '0';
			count++;
			result=0;
		}
	count++;
	transmissionSpeed = xn ;
	//CgrContact.confidence = 1;
	if (result == 0)
	{
		// Try to add contact
		//result = add_contact_to_list(CgrContact.fromNode, CgrContact.toNode, CgrContact.fromTime,
		//		CgrContact.toTime, CgrContact.xmitRate, CgrContact.confidence, 0, NULL);
		if(startTime>=0 && endTime>startTime && transmissionSpeed>0)
		result = add_contact_to_list(startTime, endTime, from, to, transmissionSpeed);
		if(result >= 1)
		{
			result = 1;
		}

	}
	else
	{
		result = -3;
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      add_range
 *
* \brief  This function parses a string to get data of a range and pass them to
 * 			the add_range_to_list function
 *
 * \details This function was adapted from the Unibo-CGR/DTN2 interface made by Giacomo Gori
 *
 *
 * \par Date Written:
 *      02/07/20
 *
 * \return int
 *
 * \retval   1   Range added
 * \retval   0   Range's arguments error
 * \retval  -1   fileline is not properly formatted
 * \retval  -2   fileline doesn't start with "+"
 *
 * \param[in]  char*		The line that contains the range
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  02/07/20 | G. Gori		    |  Initial Implementation and documentation.
 *  22/03/21 | Gabriele Nunziati	| Support for ContactPlanManager
 *  21/03/21 | Federico Le Pera|  Support for use by interface_unibocgr_dtn2.cc
 *****************************************************************************/
int ContactPlanManager::add_range(char* fileline)
{
	oasys::ScopeLock l(&lock_, "ContactPlanManager::add_range");

	//Range CgrRange;
	int result = -1;
	int n = 0;
	int count = 8;
	unsigned int ow = 0;
	long long fn = 0;
	unsigned long long from, to;
	time_t startTime, endTime;
	unsigned int delay;
	//fromTime
	if(fileline[count] == '+')
	{
		count++;
		while(fileline[count] >= 48 && fileline[count] <= 57) {
			n = n * 10 + fileline[count] - '0';
			count++;
		}
		count++;
		startTime = n;
		n=0;
	}
	else result = -2;
	//toTime
	if(fileline[count] == '+')
	{
		count++;
		while(fileline[count] >= 48 && fileline[count] <= 57) {
			n = n * 10 + fileline[count] - '0';
			count++;
		}
		count++;
		endTime = n;
		n=0;
	}
	else result = -2;
	//fromNode
	while(fileline[count] >= 48 && fileline[count] <= 57) {
			fn = fn * 10 + fileline[count] - '0';
			count++;
		}
	count++;
	from = fn ;
	fn=0;
	//toNode
	while(fileline[count] >= 48 && fileline[count] <= 57) {
			fn = fn * 10 + fileline[count] - '0';
			count++;
		}
	count++;
	to = fn ;
	//owlt
	while(fileline[count] >= 48 && fileline[count] <= 57) {
			ow = ow * 10 + fileline[count] - '0';
			count++;
			result = 0;
		}
	count++;
	delay = ow ;
	if (result == 0)
	{
		//result = add_range_to_list(CgrRange.fromNode, CgrRange.toNode, CgrRange.fromTime, CgrRange.toTime,
		//		CgrRange.owlt);
		if(startTime>=0 && endTime>startTime)
		result = add_range_to_list(startTime, endTime, from, to, delay);
		if(result >= 1)
		{
			result = 1;
		}
	}
	else
	{
		result = -1;
	}
	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      read_file_contactranges
 *
 * \brief  Read and add all new contacts or ranges of the file with filename
 * 		  specified in the first parameter to the
 *         contacts graph of thic CGR's implementation.
 *
 * \details Only for Registration and Scheduled contacts.
 *
 *
 * \par Date Written:
 *      02/07/20
 *
 * \return int
 *
 * \retval  ">= 0"  Number of contacts added to the contacts graph
 * \retval     -1   open file error
 *
 * \param[in]   char*	The name of the file to read
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  02/07/20 | G. Gori         |  Initial Implementation and documentation.
 *  20/03/21 | G. Nunziati     |  Support for use in the ContactPlanManager class
 *  20/03/22 | Federico Le Pera|  Support for use by interface_unibocgr_dtn2.cc
 *  15/04/22 | Federico Le Pera|  Support for managing the loading of ranges and contacts
 *  						   |  from lists to structures
 *****************************************************************************/
int ContactPlanManager::read_file_contactranges(char * filename)
{
	oasys::ScopeLock l(&lock_, "ContactPlanManager::read_file_contactranges");

	int result = 0, totAdded = 0, stop = 0;
	int skip = 0, count = 1;
	int result_contacts = 0, result_ranges = 0;
	int fd = open(filename, O_RDONLY);
	char car;
	if(fd < 0)
	{
		result = -1;
	}
	else
	{
		while(read(fd,&car,sizeof(char)) > 0)
		{
			if(car == '#') skip = 1;
			if(car == '\n')
			{
				if(skip==0)
				{
					lseek(fd,-count, 1);
					char * res;
					res = (char*) malloc(sizeof(char)*count);

					read(fd,res,sizeof(char)*count);
					//Recognize if it's enough lenght and if it is a contact or range
					if(count > 18 && res[0] == 'a')
					{
						if(res[2] == 'c' && res[3] == 'o' && res[4] == 'n' && res[5] == 't' && res[6] == 'a' && res[7] == 'c' && res[8] == 't')
						{
							result = add_contact(res);
							if(result == 1)
							{
								result_contacts++;
							}
						}
						else if(res[2] == 'r' && res[3] == 'a' && res[4] == 'n' && res[5] == 'g' && res[6] == 'e')
						{
							result = add_range(res);
							if(result == 1)
							{
								result_ranges++;
							}
						}
					}
					count = 0;
					free(res);
				}
				else if(skip==1)
				{
					count=0;
					skip=0;
				}
			}
			count++;
		}
		close(fd);
	}
	result = result_contacts + result_ranges;
	return result;
}
/******************************************************************************
 *
 * \par Function Name:
 *      areListUpdated
 *
 * \brief  This function check if Lists of ContactPlanManager are updated compare to the last update of
 * 			contact-plan
 *
 * \par Date Written:
 *      30/04/22
 *
 * \return bool
 *
 * \retval true if the list are updated
 * \retval false if is not
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  30/04/22 | Federico Le Pera|  Initial Implementation and documentation.
 *****************************************************************************/
bool ContactPlanManager::areListUpdated()
{
	struct stat st;
	stat("contact-plan.txt",&st);
	if(st.st_mtime>lastUpdateList.tv_sec)
	{
		return false;
	}
	return true;
}
}




/** \file ContactPlanTimer.cc
 *
 *  \brief This file provides the implementation of the functions
 *            to support the launch of a new Contact Plan Up event when
 *            there is an opening contact from the local node
 *
 ** \copyright Copyright (c) 2021, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of Unibo-DTNME-Extensions.                                            <br>
 **                                                                               <br>
 **    Unibo-DTNME-Extensions is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-DTNME-Extensions is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.
 *
 *  \author Gabriele Nunziati, gabriele.nunziati2@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */

#include "ContactPlanManager.h"
#include "bundling/BundleDaemon.h"

namespace dtn {

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanTimer::ContactPlanTimer
 *
 * \brief  Constructor of the ContactPlanTimer class
 *
 * \par Date Written: 15/09/21
 *
 * \return  *ContactPlanTimer Pointer to the class instance
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  15/09/21 | Gabriele Nunziati	|
 *****************************************************************************/

//----------------------------------------------------------------------
ContactPlanTimer::ContactPlanTimer()
{
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanTimer::start
 *
 * \brief This function starts the timer and schedules it to the DTNME
 * 			Shared Timer System if there is an opening contact in the future
 *
 *\details This function calculates when the timer must be awaken: when there is a contact
 *			opening. If there is none, it does nothing and there is no scheduling
 *
 * \par Date Written: 15/09/21
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  15/09/21 | Gabriele Nunziati	|
 *****************************************************************************/

//----------------------------------------------------------------------
void
ContactPlanTimer::start(void)
{
    ContactPlanManager *_CPManager = ContactPlanManager::instance();
	//Get when is the next opening contact
	//if there is no opening contact in the future, stop
	int nextContact = _CPManager->get_next_start_time();
	if (nextContact == -1)
		return;

	int now = _CPManager->get_seconds_from_time_zero();

	//Make sure the next opening contact is in the future
	if(nextContact > now) {
		int milliseconds = (nextContact - now) * 1000;

		//Get the opening contacts
		opening_contacts = _CPManager->get_opening_contacts(nextContact);

		std::shared_ptr<ContactPlanTimer> sptr_1(this);
		oasys::SPtr_Timer sptr_ = sptr_1;
		oasys::SharedTimer::schedule_in(milliseconds, sptr_);

		sptr_timer = sptr_;
	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanTimer::cancel
 *
 * \brief This function cancel the timer if needed
 *
 * \par Date Written: 15/09/21
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  15/09/21 | Gabriele Nunziati	|
 *****************************************************************************/

//----------------------------------------------------------------------
void
ContactPlanTimer::cancel()
{
    if (sptr_timer != nullptr) {
        oasys::SharedTimer::cancel_timer(sptr_timer);
        sptr_timer = nullptr;
    }
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactPlanTimer::timeout
 *
 * \brief This function is called when the timer stops. For each opening contact opening in
 * 			this moment, a new Contact Plan Up Event is launched to the system by the daemon
 *
 * \par Date Written: 15/09/21
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  15/09/21 | Gabriele Nunziati	|
 *****************************************************************************/

//----------------------------------------------------------------------
void
ContactPlanTimer::timeout(const struct timeval& now)
{
    ContactPlanManager *_CPManager = ContactPlanManager::instance();
	std::list<CPContact>::iterator itList;
	dtn::ContactPlanUpEvent * event;

	int from, to, startTime;
	for(itList = opening_contacts.begin(); itList != opening_contacts.end(); itList++) {
		to = (*itList).getTo();

		if(to > 0) {
			event = new ContactPlanUpEvent(to);
			BundleDaemon::post(event);
		}
	}

	_CPManager->new_timer();

    sptr_timer = nullptr;

}

}



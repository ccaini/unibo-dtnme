# Unibo-DTNME-CPM
The Unibo-DTNME-CPM  project aims to add the following features to the DTNME Bundle protcol implementation (a DTN2 fork by NASA-MSFC):
- management of contacts and ranges read from a contact-plan file in ION format 
- support of scheduled contacts; bundles are transmitted to the proximate node selected by routing only when a contact to this node has started

Unibo-DTNME-CPM is a dependency of Unibo-CGR, as the new Unibo-CGR interface to DTNME makes use 
of the contact plan management provided by Unibo-DTNME-CPM.
However, the current version of Unibo-DTN-CPM (released on 8 December 2022) can be compiled independently of Unibo-CGR, 
and can be used with the "static" router, as well as with Unibo-CGR.

Unibo-DTNME-CPM files of this version are for DTNME v1.2.0_Beta (Nov.2022). 

#### Installing
If you plan to use Unibo-DTNME-CPM with Unibo-CGR, the best way is to install the former as a dependency of the latter, by following the README instructions of Unibo-CGR.

Otherwise, to install Unibo-DTNME-CPM alone:
- use the script move_unibo_cpm_dtnme.sh (see its help) to insert it in DTNME (v1.2.0Beta)
- (re-)compile and install DTNME the usual way. 
Note that if DTNME had already been compiled, we suggest to use the "make" command after using our script, to skip the time consuming rebuilding of DTNME components left unaltered by Unibo-DTNME-CPM. 


#### Further instructions:
   
1. Once (re-)compiled and installed DTNME, please add (or check the presence) of the following instructions in the DTNME configuration file (e.g. /etc/dtnme_daemon.cfg): 
-  route set type static (or route set type uniboCGR)
-  route local_eid_ipn ipn:X.0 (to set the additional ipn address of the local node) 
-  param set announce_ipn true (to tell TCPCL to announce the "ipn" EID of the local node, instead of the deafult "dtn" EID)

2. Check that ranges and contacts are correctly read everytime a new contact plan is used. For debug purposes, if the route type is static ranges and contacts read by CPM are written in the "contact-debug.txt" file; otherwise they are included in UniboCGR logs, in the cgr_log directory.

3. Be aware that:
- Scheduled contacts can be used only with "ipn" EIDs.
- To send a bundle from A to B, it is now necessary to have a contact from A to B. To get rid of contact intermittency, when no wanted, it is enough to insert a very long contact (see below) in the contact plan. 


#### Additional remarks on ranges and contacts:
In the contact plan nodes are represented by numbers, in accordance with the ipn scheme. Times are differential with respect to "time 0", which at present is the dtnme daemon start. 
E.g.:
- a range +100 +300 101 203 5    (in the interval [+100,+300] the range from node 101 to 203 is 5s
- a contact +150 +200 101 203 1250000 (in the interval [+150,+200] there is a contact from node 101  to node 203, whose nominal Tx rate is 1250000 byte/s, i.e. 10 Mbit/s)

Remember that both ranges and contacts are unidirectional.

Contacts whose first node is the local node are "local". They are defined, as all the other contacts of the contact plan, at BP layer, not at CL one. This means, for exemple, that while a tcpcl "link" between local node 104 and 203 can be set as ALWAYSON, bundles are transmitted on the link only when stated by a contact of the Unibo-DTNME contact plan. For DTNME experts, this policy is enforced by temporarily blocking the passage of bundles from the delayed queue to the link queue, until a contact starts.

#### Unibo-DTNME-CPM adds or overwrites the following files 

1. contact_plan (new)
- /servlib/contact_plan/ContactPlanManager.cc
- /servlib/contact_plan/ContactPlanManager.h
- /servlib/contact_plan/ContactPlanTimer.cc
- /servlib/contact_plan/ContactPlanTimer.h
- /servlib/contact_plan/CPContact.cc
- /servlib/contact_plan/CPContact.h
- /servlib/contact_plan/CPRange.cc
- /servlib/contact_plan/CPRange.h

2. bundling (overwrite)
- /servlib/bundling/BundleEvent.h
- /servlib/bundling/BundleEventHandler.cc
- /servlib/bundling/BundleEventHandler.h

3. router (overwrite)
- /servlib/routing/TableBasedRouter.cc
- /servlib/routing/TableBasedRouter.h

#### Credits
Gabriele Nunziati, Federico Le Pera, Lorenzo Persampieri (authors)
Carlo Caini (supervisor), carlo.caini@unibo.it

#### Copyright
Copyright (c) 2022, University of Bologna. Auxiliary files maintain their original copyright.

#### License
Unibo-DTNME-CPM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Unibo-DTNME-CPM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Unibo-DTNME. If not, see http://www.gnu.org/licenses/.
Auxiliary files maintain their original licences.


